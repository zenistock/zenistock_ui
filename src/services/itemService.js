import axiosInstance from "./axiosInstance";

export const getAllItems = async () => {
  try {
    const response = await axiosInstance.get("api/items");
    return response.data;
  } catch (error) {
    console.error(error);
  }
};
export const createItem = async (item) => {
  try {
    const response = await axiosInstance.post("api/items", item);
    return response;
  } catch (error) {
    console.error(error);
  }
};
export const updateItem = async (item) => {
  try {
    const response = await axiosInstance.put(`api/items/${item.serial}`, item);
    return response;
  } catch (error) {
    console.error(error);
  }
};
export const deleteItem = async ({ serial }) => {
  try {
    const response = await axiosInstance.delete(`api/items/${serial}`);
    return response;
  } catch (error) {
    console.error(error);
  }
};
export const findItemById = async (serialNumber) => {
  try {
    const response = await axiosInstance.get(`api/items/${serialNumber}`);
    return response;
  } catch (error) {
    console.error(error);
  }
};
