const BASE_API_URL = "/api";

const callApi = async (
  url,
  { method = "GET", body = undefined, headers = {} } = {}
) => {
  try {
    const response = await fetch(`${BASE_API_URL}${url}`, {
      method,
      body:
        typeof body === "object" && method !== "GET"
          ? JSON.stringify(body)
          : undefined,
      headers: { "Content-type": "application/json", ...headers },
    });

    if (!response.ok) {
      const error = await response.json();
      return { error: error.code };
    }

    if (response.headers.get("Content-Type") !== "application/json") {
      const resposeText = await response.text();
      return { error: resposeText };
    }

    return await response.json();
  } catch (err) {
    return { error: err };
  }
};

export default callApi;
