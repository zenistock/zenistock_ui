import axiosInstance from "./axiosInstance";

export const getAllUsers = async () => {
  try {
    const response = await axiosInstance.get("api/users");
    return response.data;
  } catch (error) {
    console.error(error);
  }
};

export const createUser = async (user) => {
  try {
    const response = await axiosInstance.post("api/users/", user);
    return response;
  } catch (error) {
    console.error(error);
  }
};
