import React from "react";
import PropTypes from "prop-types";
import "./Divider.css";
const Divider = ({ text }) => {
  return <div className="separator">{text}</div>;
};
Divider.propTypes = {
  text: PropTypes.string,
};
export default Divider;
