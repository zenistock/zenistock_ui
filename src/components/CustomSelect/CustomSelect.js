import React from "react";
import PropTypes from "prop-types";
import { MenuItem, Select, InputLabel, FormControl } from "@material-ui/core";
import "./CustomSelect.css";

const CustomSelect = ({ selectTitle, menuItems }) => {
  return (
    <>
      <FormControl variant="outlined" className="customDimensions" size="small">
        <InputLabel> {selectTitle}</InputLabel>
        <Select label={selectTitle} defaultValue="">
          {menuItems.map((item) => (
            <MenuItem key={item.key} value={item.value}>
              {item.value}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </>
  );
};
CustomSelect.propTypes = {
  selectTitle: PropTypes.string.isRequired,
  menuItems: PropTypes.array.isRequired,
};
export default CustomSelect;
