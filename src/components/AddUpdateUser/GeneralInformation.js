import React from "react";
import { StylesProvider } from "@material-ui/core/styles";
import { Grid, Box } from "@material-ui/core";
import CustomTextField from "../CustomTextField/CustomTextField";

const GeneralInformation = () => {
  return (
    <StylesProvider injectFirst>
      <Grid container item xs={4}>
        <Box py={1.5}>
          <CustomTextField label="First name" id="name_field" />
        </Box>
        <Box py={1.5}>
          <CustomTextField label="Last name" id="serial_no_field" />
        </Box>
        <Box py={1.5}>
          <CustomTextField label="E-mail" id="invoice_no_field" />
        </Box>
      </Grid>
    </StylesProvider>
  );
};

export default GeneralInformation;
