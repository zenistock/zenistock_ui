import React from "react";
import { Collapse, Box } from "@material-ui/core";
import PropTypes from "prop-types";
import Expanded from "../Expanded";
import { colNames, colInfo } from "../Constants";

const CollapsableTablePanel = ({ expanded }) => {
  return (
    <Collapse in={expanded} timeout="auto">
      <Box margin={3}>
        <Expanded
          title="Smth random"
          columnNames={colNames}
          columnCells={colInfo}
        />
      </Box>
    </Collapse>
  );
};

CollapsableTablePanel.propTypes = {
  expanded: PropTypes.bool.isRequired,
};
export default CollapsableTablePanel;
