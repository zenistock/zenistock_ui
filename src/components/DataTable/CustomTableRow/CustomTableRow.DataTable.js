import React, { useState } from "react";
import { TableRow, TableCell, IconButton, Checkbox } from "@material-ui/core";
import Dropdown from "../../DropdownMenu/Dropdown";
import CollapsableTablePanel from "../CollapsableTablePanel.DataTable";
import PropTypes from "prop-types";
import { ExpandMore, ExpandLess } from "@material-ui/icons";
import CustomText from "../../CustomText/CustomText";

import "./CustomTableRow.css";

const CustomTableRow = ({
  row,
  rowIndex,
  dropdownItems,
  dataColumns,
  refreshFunction,
  deleteFunction,
}) => {
  const [expanded, setExpanded] = useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const handleRowClick = (event, rowIndex) => {
    console.log(event, rowIndex);
  };

  return (
    <>
      <TableRow
        hover
        onClick={(clickEvent) => handleRowClick(clickEvent, `Row-${rowIndex}`)}
        className="wrap-row"
      >
        <TableCell padding="checkbox">
          <IconButton onClick={handleExpandClick} aria-expanded={expanded}>
            {expanded ? <ExpandLess /> : <ExpandMore />}
          </IconButton>
        </TableCell>

        <TableCell padding="checkbox">
          <Checkbox> </Checkbox>
        </TableCell>

        {dataColumns.map((column, cellIndex) => (
          <TableCell key={`TableCell-${cellIndex}`}>
            {column.key === "status" ? (
              <CustomText text={row[column.key]} />
            ) : (
              <span className="word-wrap">{row[column.key]}</span>
            )}
          </TableCell>
        ))}

        <TableCell padding="checkbox">
          <Dropdown
            menuItems={dropdownItems}
            item={row}
            refreshFunction={refreshFunction}
            deleteFunction={deleteFunction}
          ></Dropdown>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell colSpan={12} className="tableCellStyle">
          <CollapsableTablePanel expanded={expanded} />
        </TableCell>
      </TableRow>
    </>
  );
};
CustomTableRow.propTypes = {
  expanded: PropTypes.bool,
  row: PropTypes.object,
  rowIndex: PropTypes.number.isRequired,
  dropdownItems: PropTypes.array.isRequired,
  dataColumns: PropTypes.array.isRequired,
  refreshFunction: PropTypes.func,
  deleteFunction: PropTypes.func,
};
export default CustomTableRow;
