import React from "react";
import TablePaginationActions from "./TablePaginationActions.DataTable";
import TablePagination from "@material-ui/core/TablePagination";
import PropTypes from "prop-types";

const CustomTablePagination = ({
  dataRows,
  rowsPerPage,
  setPage,
  setRowsPerPage,
  page,
}) => {
  const handleChangePage = (e, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (e) => {
    setRowsPerPage(parseInt(e.target.value, 10));
    setPage(0);
  };
  return (
    <TablePagination
      rowsPerPageOptions={[5, 10, 25, { label: "All", value: -1 }]}
      colSpan={12}
      count={dataRows.length}
      rowsPerPage={rowsPerPage}
      page={page}
      SelectProps={{
        inputProps: {
          "aria-label": "rows per page",
        },
        native: true,
      }}
      onPageChange={handleChangePage}
      onRowsPerPageChange={handleChangeRowsPerPage}
      ActionsComponent={TablePaginationActions}
    />
  );
};
CustomTablePagination.propTypes = {
  dataColumns: PropTypes.array.isRequired,
  dataRows: PropTypes.array.isRequired,
  setRowsPerPage: PropTypes.func.isRequired,
  setPage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};
export default CustomTablePagination;
