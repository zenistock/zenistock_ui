import React from "react";
import PropTypes from "prop-types";
import { Box, IconButton } from "@material-ui/core";
import {
  FirstPage as FirstPageIcon,
  LastPage as LastPageIcon,
  KeyboardArrowLeft,
  KeyboardArrowRight,
} from "@material-ui/icons";
import "./TablePagination.css";
const TablePaginationActions = (props) => {
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonEvent = (e) => {
    onPageChange(e, 0);
  };
  const handleBackPageButtonEvent = (e) => {
    onPageChange(e, page - 1);
  };

  const handleNextPageButtonEvent = (e) => {
    onPageChange(e, page + 1);
  };
  const handleLastPageButtonEvent = (e) => {
    onPageChange(e, Math.ceil(count / rowsPerPage) - 1);
  };
  return (
    <Box display="flex">
      <IconButton
        onClick={handleFirstPageButtonEvent}
        disabled={page === 0}
        aria-label="first page"
        className="stylize left-margin"
      >
        <FirstPageIcon />
      </IconButton>
      <IconButton
        onClick={handleBackPageButtonEvent}
        disabled={page === 0}
        aria-label="previous page"
        className="stylize"
      >
        <KeyboardArrowLeft />
      </IconButton>
      <IconButton
        onClick={handleNextPageButtonEvent}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
        className="stylize"
      >
        <KeyboardArrowRight />
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonEvent}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
        className="stylize"
      >
        <LastPageIcon />
      </IconButton>
    </Box>
  );
};
TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};
export default TablePaginationActions;
