import React, { useState } from "react";
import PropTypes from "prop-types";
import {
  Table,
  TableBody,
  TableRow,
  Container,
  Paper,
  TableFooter,
  Grid,
} from "@material-ui/core";
import TablePagination from "./TablePagination/CustomTablePagination.DataTable";
import EnhancedTableHead from "./EnhancedTableHead/EnhancedTableHead.DataTable";
import CustomTableRow from "./CustomTableRow/CustomTableRow.DataTable";
import { SORT_TYPES } from "../Constants";

const DataTable = ({
  dataColumns,
  dataRows,
  optionRows,
  sortBy,
  refreshFunction,
  deleteFunction,
}) => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [order, setOrder] = useState(SORT_TYPES.ASCENDENT);
  const [orderBy, setOrderBy] = useState(sortBy);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === SORT_TYPES.ASCENDENT;
    setOrder(isAsc ? SORT_TYPES.DESCENDENT : SORT_TYPES.ASCENDENT);
    setOrderBy(property);
  };

  const descendingComparator = (a, b, orderBy) => {
    if (b[orderBy].toLowerCase() < a[orderBy].toLowerCase()) {
      return -1;
    }
    if (b[orderBy].toLowerCase() > a[orderBy].toLowerCase()) {
      return 1;
    }
    return 0;
  };

  const getComparator = (order, orderBy) => {
    return order === SORT_TYPES.DESCENDENT
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
  };

  const stableSort = (array, comparator) => {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) return order;
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
  };

  return (
    <Grid item xs={12}>
      <Container component={Paper} disableGutters>
        <Table>
          <EnhancedTableHead
            dataColumns={dataColumns}
            order={order}
            orderBy={orderBy}
            onRequestSort={handleRequestSort}
          />
          <TableBody>
            {(rowsPerPage > 0
              ? stableSort(dataRows, getComparator(order, orderBy)).slice(
                  page * rowsPerPage,
                  page * rowsPerPage + rowsPerPage
                )
              : dataRows
            ).map((row, rowIndex) => (
              <CustomTableRow
                row={row}
                rowIndex={rowIndex}
                dropdownItems={optionRows}
                dataColumns={dataColumns}
                key={`Row-${rowIndex}`}
                refreshFunction={refreshFunction}
                deleteFunction={deleteFunction}
              ></CustomTableRow>
            ))}
          </TableBody>

          <TableFooter>
            <TableRow>
              <TablePagination
                dataColumns={dataColumns}
                dataRows={dataRows}
                rowsPerPage={rowsPerPage}
                setPage={setPage}
                setRowsPerPage={setRowsPerPage}
                page={page}
              />
            </TableRow>
          </TableFooter>
        </Table>
      </Container>
    </Grid>
  );
};

DataTable.propTypes = {
  dataColumns: PropTypes.array.isRequired,
  dataRows: PropTypes.array.isRequired,
  optionRows: PropTypes.array.isRequired,
  sortBy: PropTypes.string.isRequired,
  refreshFunction: PropTypes.func,
  deleteFunction: PropTypes.func,
};

export default DataTable;
