import React from "react";
import PropTypes from "prop-types";
import {
  TableHead,
  TableRow,
  TableCell,
  TableSortLabel,
} from "@material-ui/core";

import { ArrowDropUp } from "@material-ui/icons";

import { SORT_TYPES } from "../../Constants";

import "./EnhancedTableHead.css";

const EnhancedTableHead = (props) => {
  const { dataColumns, order, orderBy, onRequestSort } = props;

  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };
  return (
    <TableHead>
      <TableRow>
        <TableCell />
        <TableCell />
        {dataColumns.map((column, columnIndex) => (
          <TableCell
            key={`HeaderCell-${columnIndex}`}
            className="headerFontWeight"
          >
            {column.label}
            <TableSortLabel
              active={true}
              direction={orderBy === column.key ? order : SORT_TYPES.ASCENDENT}
              onClick={createSortHandler(column.key)}
              IconComponent={ArrowDropUp}
            ></TableSortLabel>
          </TableCell>
        ))}
        <TableCell />
      </TableRow>
    </TableHead>
  );
};
EnhancedTableHead.propTypes = {
  dataColumns: PropTypes.array.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf([SORT_TYPES.ASCENDENT, SORT_TYPES.DESCENDENT])
    .isRequired,
  orderBy: PropTypes.string.isRequired,
};
export default EnhancedTableHead;
