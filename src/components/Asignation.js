import React from "react";
import PropTypes from "prop-types";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

const Asignation = ({ children }) => {
  return (
    <Grid container>
      <Grid item xs={12}>
        <Typography variant="h6" gutterBottom color="textSecondary">
          {children}
        </Typography>
      </Grid>
    </Grid>
  );
};
Asignation.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Asignation;
