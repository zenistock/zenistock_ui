import React from "react";
import { Link as RouterLink } from "react-router-dom";
import Button from "@material-ui/core/Button";
import PropTypes from "prop-types";

const SideMenuButton = ({ link, text, children }) => (
  <div className="side_menu_div">
    <RouterLink to={link} className="side_menu_div_link">
      <Button className="side_menu_div_button" disableRipple>
        {children}
        <p className="side_menu_div_text">{text}</p>
      </Button>
    </RouterLink>
  </div>
);
SideMenuButton.propTypes = {
  link: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  children: PropTypes.any.isRequired,
};
export default SideMenuButton;
