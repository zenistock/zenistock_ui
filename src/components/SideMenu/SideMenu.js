import React from "react";
import { StylesProvider } from "@material-ui/core/styles";
import { ReactComponent as DashboardIcon } from "../../resources/dashboard.svg";
import { ReactComponent as DevicesIcon } from "../../resources/devices.svg";
import { ReactComponent as EmployeesIcon } from "../../resources/employees.svg";
import StoreIcon from "@material-ui/icons/Store";
import { ReactComponent as Zenicon } from "../../resources/zenicon.svg";
import { Grid } from "@material-ui/core";
import SideMenuButton from "./SideMenuButton";
import "./SideMenu.css";

const SideMenu = () => (
  <StylesProvider injectFirst>
    <Grid className="side_menu">
      <div className="logo_div">
        <Zenicon
          className="zeni_header_logo"
          onClick={() =>
            window.open("https://www.youtube.com/watch?v=SzfX9DUzwGg")
          }
        />
      </div>
      <SideMenuButton link="/dashboard" text="Dashboard">
        <DashboardIcon className="icon" />
      </SideMenuButton>
      <SideMenuButton link="/devices" text="Devices">
        <DevicesIcon className="icon" />
      </SideMenuButton>
      <SideMenuButton link="/assets" text="Assets">
        <StoreIcon className="icon" />
      </SideMenuButton>
      <SideMenuButton link="/users" text="Employees">
        <EmployeesIcon className="icon" />
      </SideMenuButton>
    </Grid>
  </StylesProvider>
);
export default SideMenu;
