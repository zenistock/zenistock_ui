import React, { useState, useEffect } from "react";

import { InputAdornment, TextField } from "@material-ui/core";
import PropTypes from "prop-types";
import "./CustomTextField.css";

const CustomTextField = ({ TextFieldIcon, label, id, value, onChange }) => {
  const [inputValue, setInputValue] = useState("");

  useEffect(() => {
    setInputValue(value);
  }, [value]);

  return (
    <TextField
      label={label}
      id={id}
      variant="outlined"
      fullWidth
      size="small"
      value={inputValue}
      onChange={onChange}
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            {TextFieldIcon || ""}
          </InputAdornment>
        ),
      }}
    ></TextField>
  );
};
CustomTextField.propTypes = {
  label: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  TextFieldIcon: PropTypes.element,
  onChange: PropTypes.func,
  value: PropTypes.string,
};
export default CustomTextField;
