import React from "react";
import CustomSelect from "../CustomSelect/CustomSelect";
import { ITEMS_CATEGORY } from "../Constants";
import Divider from "../Divider/Divider";
import Grid from "@material-ui/core/Grid";
import { Box } from "@material-ui/core";
import ComboBoxTags from "../ComboBoxTags/ComboBoxTags";

const Tags = () => {
  return (
    <>
      <Grid container item xs={4} direction="column">
        <Box py={1.5}>
          <CustomSelect selectTitle="Category" menuItems={ITEMS_CATEGORY} />
        </Box>
        <Box py={1.5}>
          <ComboBoxTags />
        </Box>
        <Box py={1.5}>
          <Divider text="Status" />
        </Box>
        <p>Available</p>
      </Grid>
    </>
  );
};

export default Tags;
