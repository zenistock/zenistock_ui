import React, { useState, useEffect } from "react";
import { StylesProvider } from "@material-ui/core/styles";
import { TextField, Grid, Box } from "@material-ui/core";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import CustomTextField from "../CustomTextField/CustomTextField";
import { DATE_FORMAT } from "../Constants";
import "./GeneralInformation.css";
import PropTypes from "prop-types";

const GeneralInformation = ({
  name,
  serial,
  invoiceNumber,
  invoiceDate,
  warrantyEndDate,
  warrantyStartDate,
  handleInputChanges,
}) => {
  const [selectedDateStart, setSelectedDateStart] = useState(Date.now());
  const [selectedDateEnd, setSelectedDateEnd] = useState(Date.now());
  const [selectedDateInvoice, setSelectedDateInvoice] = useState(Date.now());

  const [inputName, setInputName] = useState(name);
  const [inputSerial, setSerial] = useState(serial);
  const [inputInvoiceNumber, setInvoiceNumber] = useState(invoiceNumber);
  const [inputInvoiceDate, setInvoiceDate] = useState(invoiceDate);
  const [inputWarrantyStartDate, setWarrantyStartDate] =
    useState(warrantyStartDate);
  const [inputWarrantyEndDate, setWarrantyEndDate] = useState(warrantyEndDate);

  useEffect(() => {
    setInputName(name);
    setSerial(serial);
    setInvoiceNumber(invoiceNumber);
    setInvoiceDate(invoiceDate);
    setWarrantyEndDate(warrantyEndDate);
    setWarrantyStartDate(warrantyStartDate);
  }, [
    name,
    serial,
    invoiceNumber,
    invoiceDate,
    warrantyEndDate,
    warrantyStartDate,
  ]);

  const handleDateChangeStart = (date) => {
    setSelectedDateStart(date);
    handleInputChanges("warrantyStartDate", parseDate(date));
  };
  const handleDateChangeEnd = (date) => {
    setSelectedDateEnd(date);
    handleInputChanges("warrantyEndDate", parseDate(date));
  };
  const handleDateChangeInvoice = (date) => {
    setSelectedDateInvoice(date);
    handleInputChanges("invoiceDate", parseDate(date));
  };

  const parseDate = (date) => {
    let parsedDate = new Date(date);
    return (
      parsedDate.getFullYear() +
      "/" +
      parsedDate.getMonth() +
      "/" +
      parsedDate.getDate()
    );
  };

  return (
    <StylesProvider injectFirst>
      <Grid container item xs={4}>
        <Box py={1.5}>
          <CustomTextField
            label="Name"
            id="name_field"
            value={inputName}
            onChange={(e) => handleInputChanges("name", e.target.value)}
          />
        </Box>

        <Box py={1.5}>
          <CustomTextField
            label="Serial no"
            id="serial_no_field"
            value={inputSerial}
            onChange={(e) => handleInputChanges("serial", e.target.value)}
          />
        </Box>
        <p>Invoice</p>
        <Box py={1.5}>
          <CustomTextField
            label="Invoice no"
            id="invoice_no_field"
            value={inputInvoiceNumber}
            onChange={(e) =>
              handleInputChanges("invoiceNumber", e.target.value)
            }
          />
        </Box>
        <Box>
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <KeyboardDatePicker
              disableToolbar
              inputVariant="outlined"
              format={DATE_FORMAT}
              margin="normal"
              id="date-picker-invoice"
              label="Invoice date"
              value={inputInvoiceDate || selectedDateInvoice}
              onChange={(date) => handleDateChangeInvoice(date)}
              KeyboardButtonProps={{
                "aria-label": "change date",
              }}
              className="invoice_date"
            />
          </MuiPickersUtilsProvider>
        </Box>
        <p>Warranty</p>
        <Box py={1.5}>
          <Grid container item xs={12} className="warranty_div">
            <Grid
              item
              container
              xs={6}
              justifyContent="space-around"
              className="warranty_date_div"
            >
              <Box pr={1}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    disableToolbar
                    inputVariant="outlined"
                    format={DATE_FORMAT}
                    margin="normal"
                    id="date-picker-start"
                    label="Start date"
                    value={inputWarrantyStartDate || selectedDateStart}
                    onChange={(date) => {
                      handleDateChangeStart(date);
                    }}
                    KeyboardButtonProps={{
                      "aria-label": "change date",
                    }}
                  />
                </MuiPickersUtilsProvider>
              </Box>
            </Grid>

            <Grid item container xs={6} justifyContent="space-around">
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <KeyboardDatePicker
                  disableToolbar
                  inputVariant="outlined"
                  format={DATE_FORMAT}
                  margin="normal"
                  id="date-picker-end"
                  label="End date"
                  value={inputWarrantyEndDate || selectedDateEnd}
                  onChange={(date) => {
                    handleDateChangeEnd(date);
                  }}
                  KeyboardButtonProps={{
                    "aria-label": "change date",
                  }}
                />
              </MuiPickersUtilsProvider>
            </Grid>

            <Grid
              item
              container
              xs={6}
              justifyContent="space-around"
              className="warranty_date_div"
            ></Grid>
          </Grid>
        </Box>
        <Box py={1.5}>
          <TextField
            id="outlined-multiline-static"
            label="Description"
            multiline
            rows={4}
            defaultValue=""
            variant="outlined"
            className="invoice_date"
          />
        </Box>
      </Grid>
    </StylesProvider>
  );
};
GeneralInformation.propTypes = {
  defaultValues: PropTypes.array,
  handleInputChanges: PropTypes.func,
  name: PropTypes.string,
  serial: PropTypes.string,
  invoiceNumber: PropTypes.string,
  invoiceDate: PropTypes.string,
  warrantyStartDate: PropTypes.string,
  warrantyEndDate: PropTypes.string,
};
export default GeneralInformation;
