import React from "react";
import PropTypes from "prop-types";
import Grid from "@material-ui/core/Grid";

import Navigation from "../Navigation/Navigation";
import SideMenu from "../SideMenu/SideMenu";
import "./Layout.css";

const Layout = ({ children }) => (
  <Grid container className="full_page">
    <Grid container className="side_menu">
      <SideMenu />
    </Grid>
    <Grid container className="all_body">
      <Navigation />
      <Grid container className="pages">
        {children}
      </Grid>
    </Grid>
  </Grid>
);
Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
