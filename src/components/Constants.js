import React from "react";
import { Search, Edit, Delete } from "@material-ui/icons";
export const PANEL_FIELDS = [
  {
    key: "name",
    value: "Name",
    type: "textOricon",
  },

  {
    key: "serialNo",
    value: "Serial no",
    type: "textOricon",
  },
  {
    key: "tag",
    value: "Tag",
    type: "textOricon",
    icon: <Search />,
  },
  {
    key: "assignedTo",
    value: "Assigned",
    type: "textOricon",
  },
  {
    key: "select",
    value: "Status",
    type: "select",
  },
  {
    key: "invoiceNumber",
    value: "Invoice No",
    type: "textOricon",
  },
  {
    key: "invoiceDate",
    value: "Invoice Date",
    type: "date",
  },
];

export const DUMMY_DATA_ROWS = [
  {
    name: "Mac",
    invoiceNumber: "Draft",
    invoiceDate: "Draft",
    tag: "Mac",
    serial: "First Item",
    status: "Assigned",
    date: "Draft",
    warranty: "Draft",
    assign: "Dan Salagean",
  },
  {
    name: "Win",
    invoiceNumber: "Draft",
    invoiceDate: "Draft",
    tag: "Win",
    serial: "Second Item",
    status: "Assigned",
    date: "Draft",
    warranty: "Draft",
    assign: "Dan Salagean",
  },
  {
    name: "Win",
    invoiceNumber: "Draft",
    invoiceDate: "Draft",
    tag: "Win",
    serial: "Third Item",
    status: "Assigned",
    date: "Draft",
    warranty: "Draft",
    assign: "Dan Salagean",
  },
  {
    name: "iOS",
    invoiceNumber: "Draft",
    invoiceDate: "Draft",
    tag: "iOS",
    serial: "Fourth Item",
    status: "Available",
    date: "Draft",
    warranty: "Draft",
    assign: "Dan Salagean",
  },
  {
    name: "Android",
    invoiceNumber: "Draft",
    invoiceDate: "Draft",
    tag: "Android",
    serial: "Fifth Item",
    status: "Inactive",
    date: "Draft",
    warranty: "Draft",
    assign: "Dan Salagean",
  },
  {
    name: "Mac",
    invoiceNumber: "Draft",
    invoiceDate: "Draft",
    tag: "Mac",
    serial: "Sixth Item",
    status: "Assigned",
    date: "Draft",
    warranty: "Draft",
    assign: "Dan Salagean",
  },
  {
    name: "Win",
    invoiceNumber: "Draft",
    invoiceDate: "Draft",
    tag: "Win",
    serial: "Seventh Item",
    status: "Assigned",
    date: "Draft",
    warranty: "Draft",
    assign: "Dan Salagean",
  },
  {
    name: "Win",
    invoiceNumber: "Draft",
    invoiceDate: "Draft",
    tag: "Win",
    serial: "Eighth Item",
    status: "Assigned",
    date: "Draft",
    warranty: "Draft",
    assign: "Dan Salagean",
  },
  {
    name: "iOS",
    invoiceNumber: "Draft",
    invoiceDate: "Draft",
    tags: [{ tagId: 1, name: "iOS" }],
    serial: "Ninth Item",
    status: "Assigned",
    date: "Draft",
    warranty: "Draft",
    assign: "Dan Salagean",
  },
  {
    name: "Android",
    invoiceNumber: "Draft",
    invoiceDate: "Draft",
    tag: "Android",
    serial: "Tenth Item",
    status: "Assigned",
    date: "Draft",
    warranty: "Draft",
    assign: "Dan Salagean",
  },
];

export const ITEMS_DATA_COLUMNS = [
  {
    key: "name",
    label: "Name",
  },
  {
    key: "serial",
    label: "Serial nr",
  },
  {
    key: "status",
    label: "Status",
  },
  {
    key: "invoiceNumber",
    label: "Invoice no",
  },
  {
    key: "invoiceDate",
    label: "Invoice Date",
  },
  {
    key: "warrantyEndDate",
    label: "Warranty end date",
  },
  {
    key: "warrantyStartDate",
    label: "Warranty start date",
  },
  {
    key: "assign",
    label: "Assigned to",
  },
  {
    key: "tag",
    label: "Tag",
  },
];
export const USERS_DATA_COLUMNS = [
  {
    key: "email",
    label: "Email",
  },
  {
    key: "firstName",
    label: "First name",
  },
  {
    key: "lastName",
    label: "Last name",
  },
  {
    key: "role",
    label: "Role",
  },
];

export const SORT_TYPES = {
  ASCENDENT: "asc",
  DESCENDENT: "desc",
};

export const STATUS_TYPES = [
  { key: "assigned", value: "ASSIGNED" },
  { key: "available", value: "AVAILABLE" },
  { key: "inactive", value: "INACTIVE" },
];

export const DROPDOWN_COMPONENTS_USER = [
  {
    key: "1",
    name: "Edit",
    tag: <Delete fontSize="small" color="secondary" />,
  },
  {
    key: "2",
    name: "Remove",
    tag: <Delete fontSize="small" color="secondary" />,
  },
];
export const DROPDOWN_COMPONENTS = [
  {
    name: "Assign",
    tag: <Edit fontSize="small" color="secondary" />,
  },
  ...DROPDOWN_COMPONENTS_USER,
];

export const colNames = [
  "Other tags",
  "Other tags",
  "Other tags",
  "Other tags",
];
export const colInfo = [
  " Device Macbook",
  "Device Macbook",
  "Device Macbook",
  "Device Macbook",
];

export const roles = ["User", "Admin", "SuperAdmin"];
export const ITEMS_CATEGORY = [
  { key: "device", value: "Device" },
  { key: "assets", value: "Assets" },
];
export const DATE_FORMAT = "yyyy/MM/dd";
