import React from "react";

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import { StylesProvider } from "@material-ui/core/styles";
import NotificationsIcon from "@material-ui/icons/Notifications";
import Avatar from "@material-ui/core/Avatar";
import panda from "../../resources/panda.jpg";

import "./Navigation.css";

const Navigation = () => (
  <StylesProvider injectFirst>
    <AppBar position="static" className="appbar_css">
      <Toolbar className="navi">
        <span className="navi_item">
          <NotificationsIcon className="notification_icon" />
        </span>
        <span className="navi_item">
          <Avatar className="avatar" alt="My profile" src={panda} />
        </span>
      </Toolbar>
    </AppBar>
  </StylesProvider>
);

export default Navigation;
