import { MenuItem, Typography } from "@material-ui/core";
import PropTypes from "prop-types";

import React from "react";

const DropdownMenuItem = ({ key, name, tag, onClick }) => {
  return (
    <MenuItem key={key} onClick={onClick}>
      {tag}
      <Typography className="left-spacing">{name}</Typography>
    </MenuItem>
  );
};
DropdownMenuItem.propTypes = {
  key: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  tag: PropTypes.element,
  onClick: PropTypes.func.isRequired,
};
export default DropdownMenuItem;
