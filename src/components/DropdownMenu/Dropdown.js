import React, { useState } from "react";

import { Menu, IconButton } from "@material-ui/core";
import { MoreVert } from "@material-ui/icons";
import { Edit, Delete } from "@material-ui/icons";
import "./Dropdown.css";
import PropTypes from "prop-types";
import DropdownMenuItem from "./DropdownMenuItem";
import { Link } from "react-router-dom";

const Dropdown = ({ item, refreshFunction, deleteFunction }) => {
  const [anchor, setAnchor] = useState(null);
  const isOpen = Boolean(anchor);
  const handleOpen = (event) => {
    setAnchor(event.currentTarget);
  };

  const handleClose = () => {
    setAnchor(null);
  };

  return (
    <div>
      <IconButton onClick={handleOpen}>
        <MoreVert className="icon-style" />
      </IconButton>
      <Menu anchorEl={anchor} open={isOpen} keepMounted onClose={handleClose}>
        <Link
          to={{
            pathname: "/device-detail",
            state: { serialNumber: item.serial, isEditMode: true },
          }}
        >
          <DropdownMenuItem
            key="edit"
            name="Edit"
            tag={<Edit fontSize="small" color="secondary" />}
            onClick={() => {
              handleClose();
            }}
          />
        </Link>

        <DropdownMenuItem
          key="delete"
          name="Delete"
          tag={<Delete fontSize="small" color="secondary" />}
          onClick={async () => {
            handleClose();
            await deleteFunction(item);
            refreshFunction();
          }}
        />

        <DropdownMenuItem
          key="assign"
          name="Assign"
          tag={<Edit fontSize="small" color="secondary" />}
        />
      </Menu>
    </div>
  );
};
Dropdown.propTypes = {
  menuItems: PropTypes.array.isRequired,
  item: PropTypes.object.isRequired,
  refreshFunction: PropTypes.func,
  deleteFunction: PropTypes.func,
};
export default Dropdown;
