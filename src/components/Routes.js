import React from "react";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";

import Dashboard from "../pages/Dashboard";
import DeviceDetail from "../pages/DeviceDetail";
import Users from "../pages/Users";
import UsersAdd from "../pages/UsersAdd";
import Assets from "../pages/Assets";
import Devices from "../pages/Devices";

const Routes = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Redirect exact from="/" to="/dashboard" />
        <Route path="/dashboard">
          <Dashboard />
        </Route>

        <Route
          path="/device-detail"
          render={(inputProps) => <DeviceDetail inputProps={inputProps} />}
        />

        <Route path="/users">
          <Users />
        </Route>
        <Route path="/users-add">
          <UsersAdd />
        </Route>
        <Route path="/devices">
          <Devices />
        </Route>
        <Route path="/assets">
          <Assets />
        </Route>
      </Switch>
    </BrowserRouter>
  );
};

export default Routes;
