import React from "react";
import PropTypes from "prop-types";
import Alert from "@material-ui/lab/Alert";
import "./Toaster.css";
const Toaster = ({ severity, text }) => {
  return (
    <Alert variant="outlined" severity={severity} className="toaster">
      {text}
    </Alert>
  );
};
Toaster.propTypes = {
  severity: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
};

export default Toaster;
