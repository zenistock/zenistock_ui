import React from "react";

import { Typography } from "@material-ui/core";
import PropTypes from "prop-types";
import { STATUS_TYPES } from "../Constants";

import "./CustomText.css";

const CustomText = ({ text }) => {
  switch (text) {
    case STATUS_TYPES[0].value:
      return (
        <Typography className="assigned" align="center">
          {text}
        </Typography>
      );

    case STATUS_TYPES[1].value:
      return (
        <Typography className="available" align="center">
          {text}
        </Typography>
      );

    case STATUS_TYPES[2].value:
      return (
        <Typography className="inactive" align="center">
          {text}
        </Typography>
      );

    default:
      return <Typography>{text}</Typography>;
  }
};
CustomText.propTypes = {
  text: PropTypes.string.isRequired,
  color: PropTypes.string,
  backgroundColor: PropTypes.string,
};
export default CustomText;
