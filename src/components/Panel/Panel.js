import React from "react";
import { Grid, Menu, Box } from "@material-ui/core";
import PropTypes from "prop-types";
import CustomButton from "../CustomButton/CustomButton";
import CustomTextField from "../CustomTextField/CustomTextField";
import "./Panel.css";

import { STATUS_TYPES } from "../Constants";
import CustomSelect from "../CustomSelect/CustomSelect";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";

const Panel = ({ isOpen, anchor, handleClose, panelData, handleFilter }) => {
  return (
    <Menu
      open={isOpen}
      anchorEl={anchor}
      getContentAnchorEl={null}
      anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
      transformOrigin={{ vertical: "top", horizontal: "center" }}
      onClose={handleClose}
    >
      <form>
        <Grid container className="customPanelWidth">
          <Grid item xs={12}>
            <CustomButton
              buttonText="Clear all"
              customClickEvent={handleClose}
              customCSSClasses="rightPadding"
            />
          </Grid>
          {panelData.map((panelElement) => (
            <Grid item xs={12} key={panelElement.key}>
              <Box p={1} pt={1} pb={1}>
                {
                  {
                    select: (
                      <CustomSelect
                        selectTitle="Status"
                        menuItems={STATUS_TYPES}
                      />
                    ),
                    textOricon: (
                      <CustomTextField
                        id={panelElement.key}
                        TextFieldIcon={panelElement.icon}
                        label={panelElement.value}
                        handleFilter={handleFilter}
                      />
                    ),
                    date: (
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                          autoOk
                          inputVariant="outlined"
                          label="Invoice Date"
                          format="yyyy/MM/dd"
                          className="fullWidth"
                          size="small"
                        />
                      </MuiPickersUtilsProvider>
                    ),
                  }[panelElement.type]
                }
              </Box>
            </Grid>
          ))}

          <CustomButton
            buttonText="Apply"
            customClickEvent={handleClose}
            buttonVariant="red"
            customCSSClasses="rightPadding"
          />
        </Grid>
      </form>
    </Menu>
  );
};
Panel.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  panelData: PropTypes.array.isRequired,
  anchor: PropTypes.object,
  handleClose: PropTypes.func,
  handleFilter: PropTypes.func,
};
export default Panel;
