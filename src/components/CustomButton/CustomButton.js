import React from "react";

import { Button, Box, Container } from "@material-ui/core";
import PropTypes from "prop-types";
import "./CustomButton.css";

const CustomButton = ({
  buttonText,
  buttonIcon,
  buttonVariant,
  buttonMUIVariant,
  customClickEvent,
  customCSSClasses,
  type,
}) => {
  return (
    <Container disableGutters>
      <Box display="flex" flexDirection="row-reverse">
        <Button
          type={type}
          onClick={customClickEvent}
          aria-haspopup="true"
          variant={buttonMUIVariant}
          className={`${customCSSClasses} ${
            buttonVariant === "red"
              ? "redFilled redButtonDecorations"
              : "outlinedVariant outlinedVariantDecorations"
          }
            `}
        >
          {buttonIcon}
          {buttonText}
        </Button>
      </Box>
    </Container>
  );
};

CustomButton.propTypes = {
  buttonText: PropTypes.string.isRequired,
  buttonIcon: PropTypes.element,
  buttonVariant: PropTypes.string,
  buttonMUIVariant: PropTypes.string,
  customClickEvent: PropTypes.func,
  customCSSClasses: PropTypes.string,
  type: PropTypes.string,
};
export default CustomButton;
