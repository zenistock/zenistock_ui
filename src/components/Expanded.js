import React from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from "@material-ui/core";
import TableContainer from "@material-ui/core/TableContainer";
import PropTypes from "prop-types";

const Expanded = ({ title, columnNames, columnCells }) => (
  <div>
    <p>{title}</p>
    <TableContainer>
      <Table>
        <TableHead>
          <TableRow>
            {columnNames.map((value, index) => (
              <TableCell key={index} align="left">
                {value}
              </TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          <TableRow>
            {columnCells.map((value, index) => (
              <TableCell key={index} align="left">
                {value}
              </TableCell>
            ))}
          </TableRow>
        </TableBody>
      </Table>
    </TableContainer>
  </div>
);

Expanded.propTypes = {
  title: PropTypes.string.isRequired,
  columnNames: PropTypes.array.isRequired,
  columnCells: PropTypes.array.isRequired,
};

export default Expanded;
