import React, { useState, useEffect } from "react";
import Layout from "../components/Layout";

import Grid from "@material-ui/core/Grid";
import GeneralInformation from "../components/AddUpdateDevice/GeneralInformation";
import "./DeviceDetail.css";
import Divider from "../components/Divider/Divider";
import Tags from "../components/AddUpdateDevice/Tags";
import CustomButton from "../components/CustomButton/CustomButton";
import { Redirect } from "react-router-dom";
import PropTypes from "prop-types";
import { createItem, updateItem, findItemById } from "../services/itemService";

const DeviceDetail = ({ inputProps }) => {
  var itemData = [];
  const { serialNumber, isEditMode } = inputProps.location.state;
  const [redirectToDevices, setRedirectToDevices] = useState(false);
  const [values, setValues] = useState([
    {
      assign: 0,
      category: "DEVICE",
      invoiceDate: "2021/01/21",
      invoiceNumber: "",
      name: "",
      serial: "",
      status: "AVAILABLE",
      tags: [],
      warrantyEndDate: "2021/01/21",
      warrantyStartDate: "2021/01/21",
    },
  ]);

  useEffect(async () => {
    var itemPromise;
    isEditMode
      ? ((itemPromise = await findItemById(serialNumber)),
        (itemData = itemPromise.data))
      : (itemData = values);

    setValues(itemData);
  }, [serialNumber]);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const {
      invoiceDate,
      invoiceNumber,
      name,
      serial,
      warrantyEndDate,
      warrantyStartDate,
    } = values;
    const newDevice = {
      assign: null,
      category: "DEVICE",
      invoiceDate,
      invoiceNumber,
      name,
      serial,
      status: "AVAILABLE",
      tags: [],
      warrantyEndDate,
      warrantyStartDate,
    };
    console.log(newDevice);
    !isEditMode ? await createItem(newDevice) : await updateItem(newDevice);
    setRedirectToDevices(true);
  };

  const handleInputChanges = (fieldName, value) => {
    setValues({ ...values, [fieldName]: value });
  };

  return (
    <>
      <Layout>
        <h1>Device Details </h1>
        <form onSubmit={handleSubmit}>
          <Grid container>
            <Grid container item xs={12} spacing={10} className="header">
              <Grid item xs={4}>
                <Divider text="General information" />
              </Grid>
              <Grid item xs={4}>
                <Divider text="Tags" />
              </Grid>
              <Grid item xs={4}>
                <Divider text="Chestie" />
              </Grid>
            </Grid>
            <Grid container item xs={12} spacing={10}>
              <GeneralInformation
                name={values.name}
                serial={values.serial}
                invoiceNumber={values.invoiceNumber}
                invoiceDate={values.invoiceDate}
                warrantyStartDate={values.warrantyStartDate}
                warrantyEndDate={values.warrantyEndDate}
                handleInputChanges={handleInputChanges}
              />
              <Tags />
            </Grid>
          </Grid>
          <Grid container className="div_buttons">
            <span className="div_buttons_button">
              <CustomButton buttonText="Cancel" buttonMUIVariant="outlined" />
            </span>
            <span className="div_buttons_button">
              <CustomButton
                buttonText="Save device"
                buttonVariant="red"
                type="submit"
              />
            </span>
          </Grid>
          {redirectToDevices ? <Redirect to="/devices" /> : null}
        </form>
      </Layout>
    </>
  );
};
DeviceDetail.propTypes = {
  inputProps: PropTypes.object.isRequired,
};
export default DeviceDetail;
