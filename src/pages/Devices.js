import React, { useState, useEffect } from "react";

import Layout from "../components/Layout";
import DataTable from "../components/DataTable/DataTable";
import CustomButton from "../components/CustomButton/CustomButton";
import Panel from "../components/Panel/Panel";

import { Box } from "@material-ui/core";

import { ReactComponent as Filter } from "../resources/filter.svg";

import { deleteItem, getAllItems, updateItem } from "../services/itemService";
import { Link } from "react-router-dom";
import {
  ITEMS_DATA_COLUMNS,
  PANEL_FIELDS,
  DROPDOWN_COMPONENTS,
} from "../components/Constants";

const Devices = () => {
  const [openPanel, setOpenPanel] = useState(false);
  const [anchor, setAnchor] = useState(null);

  const [items, setItems] = useState([]);

  useEffect(async () => {
    const dbItems = await getAllItems();
    setItems(dbItems);
  }, [items]);

  const handleClick = (event) => {
    setAnchor(event.currentTarget);
    setOpenPanel(true);
  };

  const refreshUI = async () => {
    const dbItems = await getAllItems();
    setItems([...dbItems]);
  };

  const handleClose = () => {
    setAnchor(null);
    setOpenPanel(false);
  };
  const deleteEvent = (item) => {
    return deleteItem(item);
  };
  const updateEvent = (item) => {
    return updateItem(item);
  };

  return (
    <Layout>
      <Box pt={2} pb={2}>
        <Link
          to={{
            pathname: "/device-detail",
            state: { serialNumber: undefined, isEditMode: false },
          }}
        >
          <CustomButton buttonText="Add device" buttonVariant="red" />
        </Link>
      </Box>

      <CustomButton
        buttonText="Filter"
        buttonIcon={<Filter />}
        buttonVariant="notRed"
        customClickEvent={handleClick}
      />

      <DataTable
        dataColumns={ITEMS_DATA_COLUMNS}
        sortBy={ITEMS_DATA_COLUMNS[0].key}
        dataRows={items}
        optionRows={DROPDOWN_COMPONENTS}
        refreshFunction={refreshUI}
        deleteFunction={deleteEvent}
        updateFunction={updateEvent}
      />

      <Panel
        handleClose={handleClose}
        isOpen={openPanel}
        anchor={anchor}
        panelData={PANEL_FIELDS}
      ></Panel>
    </Layout>
  );
};

export default Devices;
