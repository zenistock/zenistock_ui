import React, { useState, useEffect } from "react";
import Layout from "../components/Layout";
import DataTable from "../components/DataTable/DataTable";
import CustomButton from "../components/CustomButton/CustomButton";

import { Box } from "@material-ui/core";
import { useHistory } from "react-router-dom";

import {
  USERS_DATA_COLUMNS,
  DROPDOWN_COMPONENTS_USER,
} from "../components/Constants";
import { getAllUsers } from "../services/callApiAxios";

const Users = () => {
  const history = useHistory();
  const navigateTo = () => history.push("/users-add");
  const [users, setUsers] = useState([]);

  useEffect(async () => {
    const dbUsers = await getAllUsers();
    setUsers(dbUsers);
  }, []);

  return (
    <Layout>
      <Box pt={2} pb={2}>
        <CustomButton
          buttonText="Add user"
          buttonVariant="red"
          customClickEvent={navigateTo}
        />
      </Box>

      <DataTable
        dataColumns={USERS_DATA_COLUMNS}
        dataRows={users}
        sortBy={USERS_DATA_COLUMNS[0].key}
        optionRows={DROPDOWN_COMPONENTS_USER}
      />
    </Layout>
  );
};
export default Users;
