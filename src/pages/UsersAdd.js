import React from "react";
import Layout from "../components/Layout";
import CustomButton from "../components/CustomButton/CustomButton";
import "./UsersAdd.css";
import GeneralInformation from "../components/AddUpdateUser/GeneralInformation";
import Divider from "../components/Divider/Divider";
import Grid from "@material-ui/core/Grid";
const UsersAdd = () => {
  function Titles() {
    return (
      <>
        <Grid item xs={4}>
          <Divider text="General information" />
        </Grid>
      </>
    );
  }
  return (
    <Layout>
      <h1>Employee Details </h1>
      <Grid container>
        <Grid container item xs={12} spacing={10} className="header">
          <Titles />
        </Grid>
        <Grid container item xs={12} spacing={10}>
          <GeneralInformation />
        </Grid>
      </Grid>
      <Grid container className="div_buttons">
        <span className="div_buttons_button">
          <CustomButton buttonText="Cancel" buttonMUIVariant="outlined" />
        </span>
        <span className="div_buttons_button">
          <CustomButton buttonText="Save employee" buttonVariant="red" />
        </span>
      </Grid>
    </Layout>
  );
};
export default UsersAdd;
