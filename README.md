# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Prerequisites

- `node@14.17.3` - (https://nodejs.org/en/)
- `yarn` - (Once you have `node` installed, install `yarn` via `$ npm install --global yarn`)

## Project setup

After cloning the repository, install project dependencies:

- `$ yarn install`

## Running the project locally

- `$ yarn start` - will start the local development server.

Once the dev server starts, you will be able to access the app on [http://localhost:3000](http://localhost:3000) in your browser. The page will also automatically reload when you make code changes.
