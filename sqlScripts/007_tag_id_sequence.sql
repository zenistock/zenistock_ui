CREATE SEQUENCE tag_id_sequence START 3;
ALTER TABLE tags ALTER COLUMN tag_id SET DEFAULT nextval('tag_id_sequence');
