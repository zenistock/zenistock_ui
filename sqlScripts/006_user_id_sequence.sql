create sequence user_id_sequence start 6;

alter table users alter column user_id set default nextval('user_id_sequence');
