ALTER TABLE devices
    RENAME TO items;
ALTER TABLE items
    ADD category varchar(10);

UPDATE items
SET category = 'DEVICE';

ALTER TABLE items
    ALTER COLUMN category
        SET NOT NULL;
