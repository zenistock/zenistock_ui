
INSERT INTO users (user_id, first_name, last_name, email, role, active)
VALUES (1, 'Laura', 'Man', 'laura.man@zenitech.co.uk', 'ADMIN', true),
       (2, 'Madalin', 'Martini', 'madalin.martini@zenitech.co.uk','SUPER_ADMIN',true),
       (3, 'Laura', 'Maxim', 'laura.maxim@zenitech.co.uk', 'SUPER_ADMIN',true),
       (4, 'Liviu', 'Cosma', 'liviu.cosma@zenitech.co.uk', 'USER',true),
       (5, 'Dan', 'Salagean', 'dan.salagean@zenitech.co.uk', 'USER',true);

INSERT INTO devices (serial_number, name, invoice_number, warranty_start_date, warranty_end_date, invoice_date, status, tags, user_id)
VALUES ('XYZ-1234-6655-AB', 'Laptop Lenovo IdeaPad L340', 'ABC12345', '2019-11-21','2022-11-21', '2019-11-20', 'ASSIGNED',  null, 1),
       ('239-GHJI-765', 'Iphone XS Max','ABC23456','2021-01-23','2022-01-23','2021-01-22','ASSIGNED',null, 2),
       ('FGH-5432-7432-LP', 'Laptop Dell Vostro 5402','ABC11111','2019-01-03','2021-01-03','2019-01-02','INACTIVE',null,null),
       ('FOP-5334-7122-OX', 'Laptop Dell Vostro 3500','ABC23458','2019-12-11','2021-12-11','2019-12-10','AVAILABLE',null,null);

