CREATE TABLE items_audit
(
    event_id      bigint PRIMARY KEY,
    operation     varchar(15) NOT NULL,
    edit_by       bigint      NOT NULL
        CONSTRAINT EDIT_fk REFERENCES users (user_id),
    stamp         timestamp   NOT NULL,
    user_id       bigint
        CONSTRAINT user_fk REFERENCES users (user_id),
    serial_number varchar(30) NOT NULL
        CONSTRAINT serial_fk REFERENCES items (serial_number)
);

CREATE INDEX serial_index
    ON items_audit (serial_number);


