
CREATE TABLE users
(
    user_id       bigint PRIMARY KEY,
    first_name    varchar(100)  NOT NULL,
    last_name     varchar(100)  NOT NULL,
    email         varchar(200) NOT NULL,
    role          varchar(15) NOT NULL,
    active        boolean NOT NULL
);


CREATE TABLE devices
(
    serial_number        varchar(30)  CONSTRAINT devices_pkey PRIMARY KEY,
    name                 varchar(100) NOT NULL,
    invoice_number       varchar(15) NOT NULL,
    warranty_start_date   timestamp NOT NULL,
    warranty_end_date     timestamp NOT NULL,
    invoice_date         timestamp NOT NULL,
    status              varchar(15) NOT NULL,
    tags                varchar(255) NULL,
    user_id              bigint NULL CONSTRAINT devices_fkey REFERENCES users(user_id)
);
