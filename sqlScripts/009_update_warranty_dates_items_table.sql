ALTER TABLE items
    ALTER COLUMN warranty_start_date DROP NOT NULL;

ALTER TABLE items
    ALTER COLUMN warranty_end_date DROP NOT NULL;
