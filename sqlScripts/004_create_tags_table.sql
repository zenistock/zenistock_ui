
CREATE TABLE tags
(
    tag_id         bigint PRIMARY KEY,
    tag_name       varchar(30) NOT NULL
);

INSERT INTO tags (tag_id, tag_name)
    VALUES ('1','laptop'),
           ('2','phone');

CREATE TABLE items_tags
(
    item_id varchar(30) CONSTRAINT items_id_fk REFERENCES items(serial_number),
    tag_id bigint CONSTRAINT tags_id_fk REFERENCES tags(tag_id),
    PRIMARY KEY (item_id, tag_id)
);
